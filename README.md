# <center>Linux编程基础</center>

---

[第一讲  Linux简介](./class1)

```
知识点
1. Unix的历史
2. Linux的发展历史
3. Linux操作系统基础知识
```

[第二讲  Ubuntu的安装](./class2)

```
知识点
1. 物理机上安装Ubuntu
2. Ubuntu的配置
3. 虚拟机上安装Ubuntu
4. 使用Docker部署开发环境
```

[第三讲  VIM和GCC](./class3)

```
知识点
1. VIM的安装
2. VIM的使用
3. VIM的配置和插件使用
4. GCC简介
5. GCC编译过程
6. GCC编译命令
```

[第四讲 Makefile](./class4)

```
知识点
1. Makefile
2. Automake
3. Cmake
4. Catkinmake
```

[第五讲 程序调试](./class5)

```
知识点
1. 单线程调试——GDB
2. 多线程调试——GDB
3. 内存错误——Valgrind
4. 内存泄漏——Valgrind
```
[第六讲 Shell](./class6)

```
知识点
1. Shell编程基础
2. Shell编程实例
3. Shell系统管理
4. Shell网络管理
```

[第七讲 Git](./class7)

```
知识点
1. Git和版本控制简介
2. Git常用命令
3. Gitlab Server
```


## Copyright

![Logo](./joint_logo.png)
